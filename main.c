// main.c

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <chopstx.h>

#include "mcu/stm32f103.h"
#include "tty.h"
#include "usb_lld.h"

#include "button.h"
#include "oled.h"
#include "spi.h"

// For set_led
#include "board.h"
#include "sys.h"

static chopstx_mutex_t mtx;
static chopstx_cond_t cnd0;
static chopstx_cond_t cnd1;

static uint8_t u, v;
static uint8_t m; // 0..100
static bool reinit = false;

// image buffer
static const uint8_t *image_1a;
static const uint8_t *image_1b;
static const uint8_t *image_1c;
static const uint8_t *image_1d;
static const uint8_t *image_2a;
static const uint8_t *image_2b;
static const uint8_t *image_2c;
static const uint8_t *image_2d;
#define IMAGES 8
static uint8_t images[IMAGES * OLED_IMAGE_SIZE];

// block in ROM for clearing the display
static const uint8_t zero_buffer[OLED_IMAGE_SIZE] = {0x00};
static const uint8_t *image_reset = zero_buffer;

static void *pwm(void *arg) {
  (void)arg;

  chopstx_mutex_lock(&mtx);
  chopstx_cond_wait(&cnd0, &mtx);
  chopstx_mutex_unlock(&mtx);

  for (;;) {
    set_led(u & v);
    chopstx_usec_wait(m);
    set_led(0);
    chopstx_usec_wait(100 - m);
    if (button_sample()) {
      image_1a = image_reset;
      image_1b = image_reset;
      image_1c = image_reset;
      image_1d = image_reset;
      image_2a = image_reset;
      image_2b = image_reset;
      image_2c = image_reset;
      image_2d = image_reset;
      memset(images, 0x00, sizeof(images));
      reinit = true;
    }
  }

  return NULL;
}

static void *blk(void *arg) {
  (void)arg;

  chopstx_mutex_lock(&mtx);
  chopstx_cond_wait(&cnd1, &mtx);
  chopstx_mutex_unlock(&mtx);

  oled_initialise(OLED_CHANNEL_1);
  oled_initialise(OLED_CHANNEL_2);

  for (;;) {

    if (reinit) {
      reinit = false;
      oled_initialise(OLED_CHANNEL_1);
      oled_initialise(OLED_CHANNEL_2);
    }

    v = 0;
    chopstx_usec_wait(200 * 1000);

    oled_send(OLED_CHANNEL_1, image_1a, OLED_IMAGE_SIZE);
    oled_send(OLED_CHANNEL_2, image_2a, OLED_IMAGE_SIZE);

    v = 1;
    chopstx_usec_wait(200 * 1000);

    oled_send(OLED_CHANNEL_1, image_1b, OLED_IMAGE_SIZE);
    oled_send(OLED_CHANNEL_2, image_2b, OLED_IMAGE_SIZE);

    v = 0;
    chopstx_usec_wait(200 * 1000);

    oled_send(OLED_CHANNEL_1, image_1c, OLED_IMAGE_SIZE);
    oled_send(OLED_CHANNEL_2, image_2c, OLED_IMAGE_SIZE);

    v = 1;
    chopstx_usec_wait(200 * 1000);

    oled_send(OLED_CHANNEL_1, image_1d, OLED_IMAGE_SIZE);
    oled_send(OLED_CHANNEL_2, image_2d, OLED_IMAGE_SIZE);
  }

  return NULL;
}

#define PRIO_PWM 3
#define PRIO_BLK 2

#define STACK_MAIN
#define STACK_PROCESS_1
#define STACK_PROCESS_2
#include "stack-def.h"
#define STACK_ADDR_PWM ((uint32_t)process1_base)
#define STACK_SIZE_PWM (sizeof process1_base)
#define STACK_ADDR_BLK ((uint32_t)process2_base)
#define STACK_SIZE_BLK (sizeof process2_base)

// main program
int main(int argc, const char *argv[]) {
  (void)argc;
  (void)argv;

  button_initialise();

  chopstx_mutex_init(&mtx);
  chopstx_cond_init(&cnd0);
  chopstx_cond_init(&cnd1);

  m = 10;

  chopstx_create(PRIO_PWM, STACK_ADDR_PWM, STACK_SIZE_PWM, pwm, NULL);
  chopstx_create(PRIO_BLK, STACK_ADDR_BLK, STACK_SIZE_BLK, blk, NULL);

  chopstx_usec_wait(200 * 1000);

  memset(images, 0xff, sizeof(images));
  image_1a = &images[0];
  image_1b = &images[0];
  image_1c = &images[OLED_IMAGE_SIZE];
  image_1d = &images[OLED_IMAGE_SIZE];
  image_2a = &images[0];
  image_2b = &images[0];
  image_2c = &images[OLED_IMAGE_SIZE];
  image_2d = &images[OLED_IMAGE_SIZE];

  memset(images, 0x00, sizeof(images));
  memset(&images[OLED_IMAGE_SIZE], 0xff, OLED_IMAGE_SIZE);

  // start background tasks
  chopstx_mutex_lock(&mtx);
  chopstx_cond_signal(&cnd0);
  chopstx_cond_signal(&cnd1);
  chopstx_mutex_unlock(&mtx);

  // tty stuff
  struct tty *tty;
  u = 1;

  tty = tty_open();
  tty_wait_configured(tty);

  m = 50;

  for (;;) {
    char s[LINEBUFSIZE];

    u = 1;
    tty_wait_connection(tty);

    chopstx_usec_wait(50 * 1000);

    // Send ZLP at the beginning
    tty_send(tty, s, 0);

    if (tty_send(tty, "display system ready\r\n", 22) < 0) {
      continue;
    }

    typedef enum {
      State_wait_open,
      State_wait_x,
      State_high_nibble,
      State_low_nibble,
      State_command,
      State_wait_close
    } State;

    uint8_t *image = &images[0];
    uint8_t command[8];

    bool execute_command = false;
    uint8_t n = 0;
    size_t offset = 0;
    size_t count = 0;

    State state = State_wait_open;

    for (;;) {
      uint32_t usec = 3000000; // microseconds
      int size = tty_recv(tty, s, &usec);

      if (size < 0) {
        break;
      } else if (0 == size) {
        continue;
      }

      if (size > 100) {
        if (tty_send(tty, ">100\r\n", 6) < 0) {
          break;
        }
      }

      for (int i = 0; i < size; ++i) {
        uint8_t c = s[i];

        switch (state) {
        default:
        case State_wait_open:
          state = State_wait_open;
          m = 50;
          switch (c) {
          case '{':
            offset = 0;
            count = 0;
            memset(image, 0, OLED_IMAGE_SIZE);
            m = 75;
            state = State_wait_x;
            break;
          case '!':
            offset = 0;
            count = 0;
            memset(command, 0, sizeof(command));
            state = State_command;
            break;
          default:
            state = State_wait_open;
            break;
          }
          break;
        case State_wait_x:
          if ('x' == c || 'X' == c) {
            state = State_high_nibble;
          } else if (';' == c || '{' == c || '#' == c) {
            state = State_wait_open;
          } else if ('}' == c) {
            state = State_wait_open;
          }
          break;
        case State_high_nibble:
          state = State_low_nibble;
          if (c >= '0' && c <= '9') {
            n = c - '0';
          } else if (c >= 'a' && c <= 'f') {
            n = c - 'a' + 10;
          } else if (c >= 'A' && c <= 'F') {
            n = c - 'A' + 10;
          } else {
            state = State_wait_open;
          }
          break;
        case State_low_nibble: {
          state = State_wait_x;
          if (c >= '0' && c <= '9') {
            n = (n << 4) + c - '0';
          } else if (c >= 'a' && c <= 'f') {
            n = (n << 4) + c - 'a' + 10;
          } else if (c >= 'A' && c <= 'F') {
            n = (n << 4) + c - 'A' + 10;
          } else {
            state = State_wait_open;
            break;
          }
          n ^= 0xff;
          image[count++] |= ((n >> 0) & 1) << offset;
          image[count++] |= ((n >> 1) & 1) << offset;
          image[count++] |= ((n >> 2) & 1) << offset;
          image[count++] |= ((n >> 3) & 1) << offset;
          image[count++] |= ((n >> 4) & 1) << offset;
          image[count++] |= ((n >> 5) & 1) << offset;
          image[count++] |= ((n >> 6) & 1) << offset;
          image[count++] |= ((n >> 7) & 1) << offset;
          if (0 == (count % OLED_ROW)) {
            ++offset;
            if (offset >= 8) { // continue to next page
              offset = 0;
              if (count >= OLED_IMAGE_SIZE) {
                state = State_wait_close;
              }
            } else {
              count -= OLED_ROW;
            }
          }
          break;
        }
        case State_wait_close:
          state = State_wait_open;
          break;

        case State_command:
          if ('$' == c) {
            execute_command = true;
            state = State_wait_open;
          } else if (offset < sizeof(command)) {
            command[offset++] = c;
            ++count;
          } else {
            state = State_wait_open;
          }
          break;
        }

        // possible commands
        // Un  - upload next XBM to image n
        // Rn  - set reset image to image n
        // Zd  - display d off
        // 1nm  - toggle OLED 1 between n and m images
        // 2nm  - toggle OLED 2 between n and m images
        //
        // Note:  n,m = 1..8
        if (execute_command) {
          execute_command = false;
          switch (command[0]) {
          case 'U':
            if (command[1] >= '1' && command[1] <= '8') {
              int n = command[1] - '1';
              image = &images[n * OLED_IMAGE_SIZE];
            }
            break;

          case 'R':
            if ('0' == command[1]) {
              image_reset = zero_buffer;
            } else if (command[1] >= '1' && command[1] <= '8') {
              int n = command[1] - '1';
              image_reset = &images[n * OLED_IMAGE_SIZE];
            }
            break;

          case 'Z':
            switch (command[1]) {
            case '1':
              image_1a = zero_buffer;
              image_1b = zero_buffer;
              image_1c = zero_buffer;
              image_1d = zero_buffer;
              break;
            case '2':
              image_2a = zero_buffer;
              image_2b = zero_buffer;
              image_2c = zero_buffer;
              image_2d = zero_buffer;
              break;
            default:
              break;
            }
            break;

          case '1':
            if ('0' == command[1]) {
              image_1a = zero_buffer;
            } else if (command[1] >= '1' && command[1] <= '8') {
              int n = command[1] - '1';
              image_1a = &images[n * OLED_IMAGE_SIZE];
            }
            if ('0' == command[2]) {
              image_1b = zero_buffer;
            } else if (command[2] >= '1' && command[2] <= '8') {
              int n = command[2] - '1';
              image_1b = &images[n * OLED_IMAGE_SIZE];
            } else {
              image_1b = image_1a;
            }
            if ('0' == command[3]) {
              image_1c = zero_buffer;
            } else if (command[3] >= '1' && command[3] <= '8') {
              int n = command[3] - '1';
              image_1c = &images[n * OLED_IMAGE_SIZE];
            } else {
              image_1c = image_1a;
            }
            if ('0' == command[4]) {
              image_1d = zero_buffer;
            } else if (command[4] >= '1' && command[4] <= '8') {
              int n = command[4] - '1';
              image_1d = &images[n * OLED_IMAGE_SIZE];
            } else {
              image_1d = image_1b;
            }
            break;

          case '2':
            if ('0' == command[1]) {
              image_2a = zero_buffer;
            } else if (command[1] >= '1' && command[1] <= '8') {
              int n = command[1] - '1';
              image_2a = &images[n * OLED_IMAGE_SIZE];
            }
            if ('0' == command[2]) {
              image_2b = zero_buffer;
            } else if (command[2] >= '1' && command[2] <= '8') {
              int n = command[2] - '1';
              image_2b = &images[n * OLED_IMAGE_SIZE];
            } else {
              image_2b = image_2a;
            }
            if ('0' == command[3]) {
              image_2c = zero_buffer;
            } else if (command[3] >= '1' && command[3] <= '8') {
              int n = command[3] - '1';
              image_2c = &images[n * OLED_IMAGE_SIZE];
            } else {
              image_2c = image_2a;
            }
            if ('0' == command[4]) {
              image_2d = zero_buffer;
            } else if (command[4] >= '1' && command[4] <= '8') {
              int n = command[4] - '1';
              image_2d = &images[n * OLED_IMAGE_SIZE];
            } else {
              image_2d = image_2b;
            }
            break;

          default:
            break;
          }
        }
      }
      u ^= 1;
    }
  }

  return 0;
}
