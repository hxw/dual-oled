// button.h

#if !defined(BUTTON_H)
#define BUTTON_H 1

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

void button_initialise(void);
bool button_sample(void);

#endif
