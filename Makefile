# Makefile

PROJECT	= dual-oled

CHOPSTX	= ../chopstx
LDSCRIPT= ${PROJECT}.ld
CSRC	= main.c usb-cdc.c button.c oled.c spi.c

ARCH	= cortex-m
CHIP	= stm32f103

USE_SYS	= yes
USE_USB	= yes
ENABLE_OUTPUT_HEX= yes

#----------------------------------------
CROSS	= arm-none-eabi-
CC	= ${CROSS}gcc
LD	= ${CROSS}gcc
OBJCOPY	= ${CROSS}objcopy

MCU	= cortex-m3
CWARN	= -Wall -Wextra -Wstrict-prototypes
DEFS	= -DUSE_SYS3 -DFREE_STANDING -DMHZ=72
OPT	= -O3 -Os -g
LIBS	=

#----------------------------------------
include ${CHOPSTX}/rules.mk

board.h:
	@echo 'Please make a symbolic link "board.h" to a file in ${CHOPSTX}/board'
	@exit 1

sys.c: board.h

distclean: clean
	rm -f board.h
