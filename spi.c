// spi.c

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include <chopstx.h>

#include "mcu/stm32f103.h"

#include "spi.h"

#define SPI_CR1_CPHA (1 << 0)
#define SPI_CR1_CPOL (1 << 1)
#define SPI_CR1_MSTR (1 << 2)
#define SPI_CR1_BR0 (1 << 3)
#define SPI_CR1_BR1 (1 << 4)
#define SPI_CR1_BR2 (1 << 5)
#define SPI_CR1_SPE (1 << 6)
#define SPI_CR1_LSBFIRST (1 << 7)
#define SPI_CR1_SSI (1 << 8)
#define SPI_CR1_SSM (1 << 9)
#define SPI_CR1_RXONLY (1 << 10)
#define SPI_CR1_DFF (1 << 11)
#define SPI_CR1_CRCNEXT (1 << 12)
#define SPI_CR1_CRCEN (1 << 13)
#define SPI_CR1_BIDIOE (1 << 14)
#define SPI_CR1_BIDIMODE (1 << 15)

#define SPI_CR2_RXDMAEN (1 << 0)
#define SPI_CR2_TXDMAEN (1 << 1)
#define SPI_CR2_SSOE (1 << 2)
#define SPI_CR2_res3 (1 << 3)
#define SPI_CR2_res4 (1 << 4)
#define SPI_CR2_ERRIE (1 << 5)
#define SPI_CR2_RXNEIE (1 << 6)
#define SPI_CR2_TXEIE (1 << 7)

#define SPI_SR_RXNE (1 << 0)
#define SPI_SR_TXE (1 << 1)
#define SPI_SR_CHSIDE (1 << 2)
#define SPI_SR_UDR (1 << 3)
#define SPI_SR_CRCERR (1 << 4)
#define SPI_SR_MODF (1 << 5)
#define SPI_SR_OVR (1 << 6)
#define SPI_SR_BSY (1 << 7)

struct SPI {
  volatile uint32_t CR1;
  volatile uint32_t CR2;
  volatile uint32_t SR;
  volatile uint32_t DR;
  volatile uint32_t CRCPR;
  volatile uint32_t RXCRCR;
  volatile uint32_t TXCRCR;
  volatile uint32_t I2SCFGR;
  volatile uint32_t I2SPR;
};

#define SPI1_BASE (PERIPH_BASE + 0x13000)
#define SPI2_BASE (PERIPH_BASE + 0x03800)
#define SPI3_BASE (PERIPH_BASE + 0x03c00)

static struct SPI *const SPI1 = (struct SPI *)SPI1_BASE;
static struct SPI *const SPI2 = (struct SPI *)SPI2_BASE;
static struct SPI *const SPI3 = (struct SPI *)SPI3_BASE;

// send a block of data as an SPI master
void spi_send(spi_channel channel, const uint8_t *buffer, size_t length) {
  struct SPI *spi = SPI1;
  switch (channel) {
  case SPI_CHANNEL_1:
    break;
  case SPI_CHANNEL_2:
    spi = SPI2;
    break;
  case SPI_CHANNEL_3:
    spi = SPI3;
    break;
  default:
    return;
  }

  // setup SPI
  spi->CR1 = SPI_CR1_MSTR |
             // SPI_CR1_BR0 | SPI_CR1_BR1 |
             SPI_CR1_SSM | SPI_CR1_SSI | SPI_CR1_CPOL | SPI_CR1_CPHA |
             SPI_CR1_SPE | SPI_CR1_BIDIMODE | SPI_CR1_BIDIOE;
  // chopstx_usec_wait(2 * 1000);

  // send data
  for (size_t i = 0; i < length; ++i) {
    spi->DR = *buffer++;
    while (0 == (spi->SR & SPI_SR_TXE)) {
    }
  }

  spi->CR1 &= ~SPI_CR1_SPE;
}
