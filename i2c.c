// i2c.c

#include <chopstx.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "mcu/stm32f103.h"

#include "i2c.h"

#define I2CPERIPH_BASE (PERIPH_BASE)

#define I2C_CR1_PE (1 << 0)
#define I2C_CR1_SMBUS (1 << 1)
#define I2C_CR1_res2 (1 << 2)
#define I2C_CR1_SMBTYPE (1 << 3)
#define I2C_CR1_ENARP (1 << 4)
#define I2C_CR1_ENPEC (1 << 5)
#define I2C_CR1_ENGC (1 << 6)
#define I2C_CR1_NOSTRETCH (1 << 7)
#define I2C_CR1_START (1 << 8)
#define I2C_CR1_STOP (1 << 9)
#define I2C_CR1_ACK (1 << 10)
#define I2C_CR1_POS (1 << 11)
#define I2C_CR1_PEC (1 << 12)
#define I2C_CR1_ALERT (1 << 13)
#define I2C_CR1_res14 (1 << 14)
#define I2C_CR1_SWRST (1 << 15)

#define I2C_CR2_FREQ 0x0000003f
#define I2C_CR2_res6 (1 << 6)
#define I2C_CR2_res7 (1 << 7)
#define I2C_CR2_ITERREN (1 << 8)
#define I2C_CR2_ITEVTEN (1 << 9)
#define I2C_CR2_ITBUFEN (1 << 10)
#define I2C_CR2_DMAEN (1 << 11)
#define I2C_CR2_LAST (1 << 12)
#define I2C_CR2_res13 (1 << 13)
#define I2C_CR2_res14 (1 << 14)
#define I2C_CR2_res16 (1 << 15)

#define I2C_OAR1_ADD0 0x00000001
#define I2C_OAR1_ADD 0x000003fe
#define I2C_OAR1_ADDMODE (1 << 15)

#define I2C_OAR2_ENDUAL (1 << 0)
#define I2C_OAR2_ADD2 0x000003fe

#define I2C_SR1_SB (1 << 0)
#define I2C_SR1_ADDR (1 << 1)
#define I2C_SR1_BTF (1 << 2)
#define I2C_SR1_ADD10 (1 << 3)
#define I2C_SR1_STOPF (1 << 4)
#define I2C_SR1_res5 (1 << 5)
#define I2C_SR1_RxNE (1 << 6)
#define I2C_SR1_TxE (1 << 7)
#define I2C_SR1_BERR (1 << 8)
#define I2C_SR1_ARLO (1 << 9)
#define I2C_SR1_AF (1 << 10)
#define I2C_SR1_OVR (1 << 11)
#define I2C_SR1_PECERR (1 << 12)
#define I2C_SR1_res13 (1 << 13)
#define I2C_SR1_TIMEOUT (1 << 14)
#define I2C_SR1_SMBALERT (1 << 15)

#define I2C_SR2_MSL (1 << 0)
#define I2C_SR2_BUSY (1 << 1)
#define I2C_SR2_TRA (1 << 2)
#define I2C_SR2_res3 (1 << 3)
#define I2C_SR2_GENCALL (1 << 4)
#define I2C_SR2_SMBDEFAULT (1 << 5)
#define I2C_SR2_SMBHOST (1 << 6)
#define I2C_SR2_DUALF (1 << 7)
#define I2C_SR2_PEC 0x0000ff00

#define I2C_CCR 0x00000fff
#define I2C_CCR_res12 (1 << 12)
#define I2C_CCR_res13 (1 << 13)
#define I2C_CCR_DUTY (1 << 14)
#define I2C_CCR_F_S (1 << 15)

#define I2C_TRISE 0x000000ff

struct I2C {
  volatile uint32_t CR1;
  volatile uint32_t CR2;
  volatile uint32_t OAR1;
  volatile uint32_t OAR2;
  volatile uint32_t DR;
  volatile uint32_t SR1;
  volatile uint32_t SR2;
  volatile uint32_t CCR;
  volatile uint32_t TRISE;
};

#define I2C1_BASE (I2CPERIPH_BASE + 0x5400)
#define I2C2_BASE (I2CPERIPH_BASE + 0x5800)

static struct I2C *const I2C1 = (struct I2C *)I2C1_BASE;
static struct I2C *const I2C2 = (struct I2C *)I2C2_BASE;

// send a block of data as an I2C master
void i2c_send(i2c_channel channel, uint8_t address, const uint8_t *buffer,
              size_t length) {
  struct I2C *i2c = I2C1;
  switch (channel) {
    case I2C_CHANNEL_1:
      break;
    case I2C_CHANNEL_2:
      i2c = I2C2;
      break;
    default:
      return;
  }

  // reset
  i2c->CR1 = I2C_CR1_SWRST;
  chopstx_usec_wait(2 * 1000);
  i2c->CR1 &= 0;

  // set frequency control
  i2c->CR2 = 0x08;

  // configure the clock control registers
  i2c->CCR = 0x28;  // 100kHz

  // configure the rise time register
  i2c->TRISE = 0x09;

  // enable i2c
  i2c->CR1 = I2C_CR1_PE;

  chopstx_usec_wait(2 * 1000);

  // start bit
  i2c->CR1 |= I2C_CR1_START;
  while (0 == (i2c->SR1 & I2C_SR1_SB)) {
  }

#if 1
  // send address + R/~W
  i2c->DR = (address << 1) | 0;  // address + R/~W=0 (write)
  while (0 == (i2c->SR1 & I2C_SR1_ADDR)) {
  }
  (void)(i2c->SR2);  // read status to reset addr
#endif

#if 0
    //    chopstx_usec_wait(2);
    GPIOB->ODR |= 0x0100;
    chopstx_usec_wait(20 * 1000);
    GPIOB->ODR &= ~0x0100;
    chopstx_usec_wait(20 * 1000);

  }
#endif

#if 0
  // send data
  for (size_t i = 0; i < length; ++i) {
    i2c->DR = *buffer++;
    while (0 == (i2c->SR1 & I2C_SR1_TxE)) {
    }
  }
#endif

  // stop bit
  i2c->CR1 |= I2C_CR1_STOP;
  while (0 != (i2c->SR2 & I2C_SR2_BUSY)) {
  }
}
