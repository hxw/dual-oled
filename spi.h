// spi.h

#if !defined(SPI_H)
#define SPI_H 1

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

typedef enum {
  SPI_CHANNEL_1,
  SPI_CHANNEL_2,
  SPI_CHANNEL_3,
} spi_channel;

void spi_send(spi_channel channel, const uint8_t *buffer, size_t length);

#endif
