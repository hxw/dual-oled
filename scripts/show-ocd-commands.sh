#!/bin/sh

ERROR() {
  printf 'error: '
  printf "$@"
  printf '\n'
  exit 1
}


this_dir=$(dirname "$0")
build="${this_dir}/../build/"

[ -d "${build}" ] || ERROR 'missing direcotrory: "%s" run "make" first' "${build}"

bin=
for b in "${build}/"*.bin
do
  [ -x "${b}" ] && bin=$(realpath "${b}") && break
done
[ -z "${bin}" ] && ERROR 'no ".bin" files in: "%s" run "make" first' "${build}"

cat <<EOF
##--------------------------------------------------

reset halt
stm32f1x unlock 0

reset halt
stm32f1x mass_erase 0
flash write_bank 0 ${bin} 0

stm32f1x lock 0
reset halt

reset run

shutdown

##--------------------------------------------------
## in another terminal run:
##     doas scripts/start-ocd
##
## in this terminal run:
##     telnet 127.0.0.1 4444
##
## paste commands into telnet session as required
EOF
