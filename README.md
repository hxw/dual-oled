# dual-oled

Dual OLED driver for STM32 board

This depends on the STM32 chopstx kernel from:

~~~
git clone https://salsa.debian.org/gnuk-team/chopstx/chopstx.git
~~~


# Commands

use tip or similar to connect to the USB serial interface

in the following list <X> represents a variable value; other letters,
digits and punctuation represent them selves.  Whitespace is ignored,
when a space is required it will be shown as <SPACE>.

==============================  ======================================================
Command Sequence                Description
==============================  ======================================================
{ 0xHH, ..., 0xHH }             load hex data - this can be 128x64 XBM file

! U <B> $                       set upload image buffer 1≤B≤8 (8 buffers)
                                next XBM upload is stored in this buffer

! R <B> $                       set reset button image buffer 0≤B≤8 (Zero + 8 buffers)
                                on pressing reset both displays show this buffer

! Z <D> $                       set display 1 or 2 as blank

! <D> <B1> <B2> <B3> <B4> $     set display 1 or 2 as toggling between B1/B2/B3/B4
                                if Bn = 0 (zero) it represents blank screen
                                if B2 absent use B1
                                if B3 absent use B1
                                if B4 absent use B2
==============================  ======================================================


# Sending text
Find a suitable font

~~~
convert -list font
~~~


use ImageMagick to create a text image

~~~
convert -background black -fill white -page 128x64 -size 128x64 -font Noto-Sans-CJK-TC-Regular -pointsize 16 label:'Hello\n你好嗎' text.xbm
~~~
