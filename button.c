// button.c

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include <chopstx.h>

#include "mcu/stm32f103.h"

#include "button.h"

static uint8_t state;
static bool pressed;

void button_initialise(void) {
  // mapping of GPIOA
  // PA0 = button
  //                           76543210      76543210
  GPIOA->CRL = (GPIOA->CRL & 0xfffffff0) | 0x00000004; // PA7..PA0
  pressed = false;
  state = 0;
}

// return true if button is pressed
bool button_sample(void) {

  state = (state << 1) | (GPIOA->IDR & 1);

  if (0 == state) {
    pressed = false;
  } else if (0xff == state) {
    pressed = true;
  }
  // else button bounce
  return pressed;
}
