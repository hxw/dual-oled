// oled.h

#if !defined(OLED_H)
#define OLED_H 1

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

typedef enum {
  OLED_CHANNEL_1,
  OLED_CHANNEL_2,
  //
  OLED_CHANNEL_COUNT
} oled_channel;

// bytes in one page
#define OLED_ROW 128

// total pages in display
#define OLED_PAGES 8

// image buffer size
#define OLED_IMAGE_SIZE (OLED_ROW * OLED_PAGES)

void oled_initialise(oled_channel channel);
void oled_send(oled_channel channel, const uint8_t *buffer, size_t length);

#endif
