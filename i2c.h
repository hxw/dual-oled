// i2c.h

#if !defined(I2C_H)
#define I2C_H 1

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

typedef enum {
  I2C_CHANNEL_1,
  I2C_CHANNEL_2,
} i2c_channel;

void i2c_send(i2c_channel channel, uint8_t address, const uint8_t *buffer,
              size_t length);

#endif
