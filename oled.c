// oled.c

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include <chopstx.h>

#include "mcu/stm32f103.h"

#include "oled.h"
#include "spi.h"

#define RCC_APB1ENR_SPI2EN (1 << 14)
#define RCC_APB1RSTR_SPI2RST (1 << 14)

#define RCC_APB2ENR_SPI1EN (1 << 12)
#define RCC_APB2RSTR_SPI1RST (1 << 12)

typedef struct {
  spi_channel spi;
  struct GPIO *gpio;
  uint16_t reset_bit;
  uint16_t cd_bit;
} oled_t;

static const oled_t oled[OLED_CHANNEL_COUNT] = {
    {.spi = SPI_CHANNEL_1,
     .gpio = GPIOB,
     .reset_bit = 0x0002,
     .cd_bit = 0x0001},
    {.spi = SPI_CHANNEL_2,
     .gpio = GPIOA,
     .reset_bit = 0x0200,
     .cd_bit = 0x0100},
};

void oled_initialise(oled_channel channel) {
  const oled_t *display;

  switch (channel) {
  case OLED_CHANNEL_1:

    display = &oled[channel];

    // enable/reset SPI1
    if (0 == (RCC->APB2ENR & RCC_APB2ENR_SPI1EN)) {
      RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;
      RCC->APB2RSTR = RCC_APB2RSTR_SPI1RST;
      RCC->APB2RSTR = 0;
    }

    // enable/reset GPIOB
    if (0 == (RCC->APB2ENR & RCC_APB2ENR_IOPBEN)) {
      RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;
      RCC->APB2RSTR = RCC_APB2RSTR_IOPBRST;
      RCC->APB2RSTR = 0;
    }

    // mapping of SPI1
    // PA7 = MOSI
    // PA6 = MISO
    // PA5 = CLK
    // PA4 = NSS
    //                           76543210      76543210
    GPIOA->CRL = (GPIOA->CRL & 0x0f0fffff) | 0xb0b00000; // PA7..PA0

    // PB1 = gpio push-pull RST
    // PB0 = gpio push-pull D/C
    //                           76543210      76543210
    GPIOB->CRL = (GPIOB->CRL & 0xffffff00) | 0x00000033; // PB7..PB0
    // GPIOB->CRH = (GPIOB->CRH & 0xffffffff) | 0x00000000;  // PB15..PB8

    break;

  case OLED_CHANNEL_2:

    display = &oled[channel];

    // enable/reset SPI2
    if (0 == (RCC->APB1ENR & RCC_APB1ENR_SPI2EN)) {
      RCC->APB1ENR |= RCC_APB1ENR_SPI2EN;
      RCC->APB1RSTR = RCC_APB1RSTR_SPI2RST;
      RCC->APB1RSTR = 0;
    }

    // enable/reset GPIOA
    if (0 == (RCC->APB2ENR & RCC_APB2ENR_IOPAEN)) {
      RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
      RCC->APB2RSTR = RCC_APB2RSTR_IOPARST;
      RCC->APB2RSTR = 0;
    }

    // mapping of SPI2
    // PB15 = MOSI
    // PB14 = MISO
    // PB13 = CLK
    // PB12 = NSS                111111        111111
    //                           54321098      54321098
    GPIOB->CRH = (GPIOB->CRH & 0x0f0fffff) | 0xb0b00000; // PB15..PB8

    // PA9 = gpio push-pull RST
    // PA8 = gpio push-pull D/C  111111        11111
    //                           54321098      54321098
    GPIOA->CRH = (GPIOA->CRH & 0xffffff00) | 0x00000033; // PA15..PA8

    break;

  default:
    return;
  }

  // pulse RST low
  display->gpio->ODR |= (display->reset_bit | display->cd_bit);
  chopstx_usec_wait(20 * 1000);
  display->gpio->ODR &= ~(display->reset_bit | display->cd_bit);
  chopstx_usec_wait(20 * 1000);
  display->gpio->ODR |= display->reset_bit;
  chopstx_usec_wait(2 * 1000);

  // configure display
  static const uint8_t c0[] = {
      0xae,       // display off
      0xd5, 0x80, // Set Display Clock Divide Ratio/Oscillator Frequency
      0xa8, 0x3f, // Set Multiplex Ratio
      0xd3, 0x00, // Set Display Offset
      0x40,       // Set Display Start Line
      0x8d, 0x14, // Set Charge Pump (internal Vcc)
      0xa1,       // Set Segment Re-Map
      0xc8,       // Set COM Output Scan Direction
      0xda, 0x12, // Set COM Pins Hardware Configuration
      0x81, 0xcf, // Set Contrast Control (internal Vcc)
      0xd9, 0xf1, // Set Pre-Charge Period (internal Vcc)
      0xdb, 0x40, // Set VCOMH Deselect Level
      // 0x2e,       // Stop scrolling
      // 0x20, 0x02, // Page Addressing Mode
      // 0xb0,       // Page 0 (8 pages: 0xb0..0xb7)
      // 0x10, 0x00, // Column 0 (page mode)
      // 0x20, 0x00, // Horizontal Addressing Mode (auto next page wrap)
      // 0x21, 0x00, 0x7f, // Setup column start and end address
      // 0x22, 0x00, 0x07, // Setup page start and end address
      0xa4, // Display RAM
      // 0xa5,       // All Pixels Forced On
      0xa6, // Set Normal Display
      // 0xa7,       // Set Inverse Display
      // 0xaf,       // display on
      0xaf, // display on
  };

  display->gpio->ODR &= ~display->cd_bit;
  spi_send(display->spi, c0, sizeof(c0));
}

void oled_send(oled_channel channel, const uint8_t *buffer, size_t length) {
  if (length < OLED_IMAGE_SIZE) {
    return;
  }
  if (channel >= OLED_CHANNEL_COUNT) {
    return;
  }
  const oled_t *display = &oled[channel];

  uint8_t address[] = {
      0xb0,      // Page Address
      0x10, 0x00 // Column Address
  };

  const uint8_t *p = buffer;
  for (int page = 0; page < OLED_PAGES; ++page) {
    address[0] = 0xb0 | page;
    display->gpio->ODR &= ~display->cd_bit;
    spi_send(display->spi, address, sizeof(address));
    display->gpio->ODR |= display->cd_bit;
    spi_send(display->spi, p, OLED_ROW);
    p += OLED_ROW;
  }
}
